(function (scope) {
  const INIT =      'i';
  const BREAK =     'b';
  const WORD =      'w';
  const OPERATOR =  '^';
  const STRING =    '$';
  const NUMBER =    '#';
  const MARK =      ' ';

  const combine = function(value1, value2) {
    if (value1 === undefined) {
      return value2;
    }
    if (typeof value1 === 'function') {
      return value1(value2);
    }
    if (value2 === undefined) {
      return value1;
    }
    throw new Error(`Cannot combine ${value1} and ${value2}`);
  };

  class Logic {
    constructor() {
      this.instructions = [];
    }

    blankState(context) {
      return {
        context: context || {},
        state: INIT,
        word: [],
        operator: undefined,
        key: undefined,
        value: undefined
      };
    }

    run(context) {
      this.state = this.blankState(context);
      this.instructions.forEach(instruction => {
        let mode = instruction[0];
        instruction = instruction.substr(1);
        let [line, col, data] = instruction.split(MARK, 3);
        try {
          if (this.state.operator && this.state.operator.length > 0) {
            if (this.state.operator in Logic.operations) {
              Logic.operations[this.state.operator](this.state);
              this.state.operator = undefined;
            }
          }
          this[mode](data);
        }
        catch(e) {
          e.message = `At ${line}:${col} - ${e.message}`;
          throw e;
        }
      });
    }

    getContextProperty(propertyNameArr) {
      let value = this.state.context;
      propertyNameArr.forEach(function (key, i) {
        if (key === 'Global' && i === 0) {
          value = scope;
        }
        else if (value === undefined || value === null) {
          throw new Error(`Cannot read property ${key} of ${value}`);
        }
        else {
          value = value[key];
        }
      });
      return value;
    }

    setContextProperty(propertyNameArr, value) {
      let setContext = this.state.context;
      let finalProp = propertyNameArr.pop();
      propertyNameArr.forEach(function (key, i) {
        if (setContext[key] === undefined || setContext[key] === null) {
          setContext[key] = {};
        }
        setContext = setContext[key];
      });
      setContext[finalProp] = value;
    }

    [INIT](argument) {
      console.log('INIT?!?!???!');
    }

    [BREAK](value) {
      if (this.state.key !== undefined && this.state.value !== undefined) {
        this.setContextProperty(this.state.key, this.state.value);
      }
      else {
        debugger;
      }
    }

    [WORD](value) {
      switch (this.state.state) {
        case INIT:
          this.state.word = [value];
          break;
        case OPERATOR:
          switch (this.state.operator) {
            case '.':
              this.state.word.push(value);
              break;
            default:
              if (this.state.word.length > 0) {
                throw new Error(`Internal error near \`${value}\``);
              }
              this.state.word = [value];
          }
          break;
        case WORD:
          this.state.value = this.getContextProperty(this.state.word);
          this.state.word = [value];
          break;
        default:
          throw new Error(`Unhandled state ${this.state.state}`);
      }
      this.state.state = WORD;
    }

    [OPERATOR](value) {
      switch (this.state.state) {
        case BREAK:
        case WORD:
        case INIT:
        case STRING:
        case NUMBER:
          this.state.operator = value;
          break;
        case OPERATOR:
          this.state.operator += value;
          break;
        default:
          throw new Error(`Unhandled state ${this.state.state}`);
      }
      this.state.state = OPERATOR;
    }

    [STRING](value) {
      this.state.value = combine(this.state.value, value);
    }

    [NUMBER](value) {
      this.state.value = combine(this.state.value, value);
    }
  }

  Logic.operations = {
    '=': function (context) {
      context.key = context.word;
      context.word = [];
    }
  };

  let descript = scope.descript = {
    root: {},
    load: function (src) {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', src);
      xhr.send(null);
      xhr.onreadystatechange = function () {
        let DONE = 4;
        let OK = 200;
        if (xhr.readyState === DONE) {
          if (xhr.status === OK) {
            descript.exec(xhr.responseText, descript.root);
          }
          else {
            throw new Error('Error loading <' + src + '>: ' + xhr.status);
          }
        }
      };
    },
    exec: function (str, context) {
      let logic = descript.parse(str);
      logic.run(context);
    },
    parse: function (str) {
      let logic = new Logic();
      let queue = '';
      let mode = INIT;
      let line = 1;
      let col = 0;
      let startLine = 1;
      let startCol = 1;
      let flush = function () {
        if (mode !== INIT) {
          logic.instructions.push('' + mode + startLine + MARK + startCol + MARK + queue);
        }
        startLine = line;
        startCol = col;
        queue = '';
        mode = INIT;
      };
      let isNum = function (code) {
        return code >= 48 && code <= 57;
      };
      let isWord = function (code) {
        return code > 127 || (code >= 65 && code <= 90) || (code >= 97 && code <= 122);
      };
      let isSpace = function (code) {
        return code === 9 || code === 32;
      };
      let isBreak = function (code) {
        return code === 10 || code === 13 || code === 59;
      };
      let isNewline = function (code) {
        return code === 10 || code === 13;
      };
      let isOperator = function (code) {
        return !isNum(code) && !isWord(code) && !isSpace(code) && !isBreak(code);
      };
      let isSingleCharOperator = function(chr) {
        return chr.length === 1 && '!@{}[]().-+*/'.indexOf(chr) !== -1;
      };
      for (let i = 0; i < str.length; i++) {
        let chr = str[i];
        let code = chr.charCodeAt(0);
        col++;
        if (isNewline(code)) {
          line++;
          col = 0;
        }
        if (chr === '\'') {
          if (mode === STRING) {
            flush();
            continue;
          }
          else {
            flush();
            mode = STRING;
            continue;
          }
        }
        if (mode === STRING) {
          queue += chr;
          continue;
        }
        switch (mode) {
          case BREAK:
            if (isBreak(code)) {
              queue += chr;
              continue;
            }
            flush();
            break;
          case WORD:
            if (isWord(code)) {
              queue += chr;
              continue;
            }
            flush();
            break;
          case OPERATOR:
            if (isOperator(code)) {
              if (isSingleCharOperator(queue) || isSingleCharOperator(chr)) {
                flush();
                mode = OPERATOR;
              }
              queue += chr;
              continue;
            }
            flush();
            break;
          case NUMBER:
            if (isNum(code)) {
              queue += chr;
              continue;
            }
            flush();
            break;
        }
        switch (mode) {
          case INIT:
            if (isBreak(code)) {
              mode = BREAK;
            }
            else if (isNum(code)) {
              mode = NUMBER;
            }
            else if (isWord(code)) {
              mode = WORD;
            }
            else if (isSpace(code)) {
              mode = INIT;
            }
            else {
              mode = OPERATOR;
            }
            startLine = line;
            startCol = col;
            queue = chr;
            break;
          default:
            throw new Error('Invalid state');
        }
      }
      return logic;
    }
  };
})(typeof module === 'object' && module.exports ? module.exports : this);
